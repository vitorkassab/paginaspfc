+++
title = 'Telê Santana'
date = 2023-10-02T02:09:54-03:00
draft = false


+++

Telê Santana é uma figura icônica no mundo do futebol, especialmente quando se trata de treinadores. Sua carreira como técnico é uma história de sucesso e sua contribuição para o esporte é inestimável. Santana é amplamente reconhecido por suas táticas inovadoras, sua paixão pelo futebol e sua capacidade de extrair o melhor de seus jogadores.

O legado de Telê Santana como treinador começou a se destacar no Brasil, onde ele comandou equipes como o Atlético Mineiro e o São Paulo FC. Sua filosofia de jogo era baseada em um futebol ofensivo e bonito de se ver. Ele acreditava no toque de bola, na criatividade e na expressão individual dos jogadores, o que resultava em times que jogavam um futebol encantador.

Uma das maiores conquistas de Telê Santana como treinador foi a Copa do Mundo de 1982, quando ele comandou a seleção brasileira. Apesar de não terem vencido o torneio, aquele time é lembrado como um dos mais talentosos e empolgantes da história do futebol, e muito disso se deve à visão de jogo de Santana. Ele trouxe um estilo de jogo ousado e atrativo que cativou fãs em todo o mundo.

Além de sua abordagem tática, Telê Santana também era conhecido por seu carisma e sua capacidade de motivar seus jogadores. Ele tinha uma maneira única de inspirar confiança e camaradagem entre a equipe, o que muitas vezes resultava em desempenhos excepcionais em campo.

Telê Santana deixou uma marca indelével no mundo do futebol como treinador. Sua influência perdura até hoje, com muitos treinadores em todo o mundo se inspirando em sua filosofia de jogo. Sua paixão, dedicação e estilo de jogo inovador tornam-no uma figura lendária no esporte, e seu nome sempre será lembrado com reverência por todos os amantes do futebol.