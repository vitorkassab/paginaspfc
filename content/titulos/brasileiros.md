+++
title = 'Campeonatos Brasileiros'
date = 2023-10-02T02:09:54-03:00
draft = false

+++



A primeira vitória ocorreu em 1977, quando o São Paulo, liderado por ícones como Zé Sergio e Serginho Chulapa, sagrou-se campeão brasileiro pela primeira vez. Esse feito histórico marcou o início de uma era vitoriosa para o clube.

Em 1986, o São Paulo FC alcançou seu segundo título nacional, com jogadores talentosos como Careca e Silas brilhando em campo. A equipe se destacou pelo futebol ofensivo e pela determinação.

O terceiro título veio em 1991, com a equipe comandada por Telê Santana, que viria a se tornar uma das mais memoráveis da história do clube. Essa conquista marcou o início da chamada "Era Telê," que trouxe grandes feitos ao São Paulo.

O São Paulo FC manteve sua supremacia no futebol brasileiro ao vencer o Campeonato Brasileiro de 2006, 2007 e 2008. Sob a liderança de Muricy Ramalho, o clube se tornou o primeiro a conquistar o Brasileirão em três edições consecutivas na era dos pontos corridos.

Esses seis títulos do Campeonato Brasileiro são parte fundamental da rica história do São Paulo FC, reafirmando sua posição como um dos clubes mais respeitados e bem-sucedidos do país. Cada conquista é um capítulo inesquecível na trajetória do clube, celebrado com paixão pelos torcedores são-paulinos.


<img src="https://1.bp.blogspot.com/_a5JsCv9dK3w/STz37aRZ0YI/AAAAAAAADMQ/awe6NUNE_kU/s400/sao-paulo-poster-hexa.jpg" alt = "Elenco HexaCampeão" id = image-br>