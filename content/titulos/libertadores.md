+++
title = 'Libertadores'
date = 2023-10-02T02:09:54-03:00
draft = false


+++

O São Paulo Futebol Clube é também reverenciado por suas três conquistas da Copa Libertadores, o principal torneio de clubes da América do Sul, que demonstram a excelência do clube no cenário continental.

A primeira conquista da Libertadores pelo São Paulo ocorreu em 1992, sob a liderança do técnico Telê Santana. O time enfrentou o Newell's Old Boys, da Argentina, na emocionante final. Após um empate sem gols no tempo regulamentar, o São Paulo venceu na disputa por pênaltis, com grande atuação do goleiro Zetti. Essa vitória marcou a estreia do clube no cenário internacional de maneira triunfal.

No ano seguinte, em 1993, o São Paulo FC provou sua consistência e qualidade ao conquistar sua segunda Copa Libertadores consecutiva. Novamente sob o comando de Telê Santana, o São Paulo derrotou o Universidad de Chile na final, assegurando seu lugar na história do futebol sul-americano.

A terceira conquista da Copa Libertadores ocorreu em 2005, quando o São Paulo FC enfrentou o Atlético Paranaense em uma final inteiramente brasileira. O São Paulo venceu por 4 a 0 na partida de volta, após um empate por 1 a 1 no primeiro jogo, garantindo seu terceiro título continental. O técnico Paulo Autuori foi o responsável por conduzir o clube a essa vitória histórica.

Essas três conquistas da Copa Libertadores destacam o São Paulo FC como um dos clubes mais bem-sucedidos do continente sul-americano e reafirmam sua presença de destaque no cenário internacional do futebol. Elas são motivo de grande orgulho para os torcedores são-paulinos e fazem parte do rico legado do clube.


<img src="https://i.pinimg.com/originals/ab/b2/36/abb236cb4410c1c968b726bd8313bbed.jpg" alt = "Rogério Ceni Tri-Libertadores" id = "imagem-libertadores">

