+++
title = 'Historia'
date = 2023-10-02T02:09:54-03:00
draft = false
+++



O São Paulo Futebol Clube, também conhecido como São Paulo FC ou simplesmente São Paulo, é um dos clubes de futebol mais tradicionais e bem-sucedidos do Brasil e do mundo. Sua história remonta ao dia 25 de janeiro de 1930, quando foi fundado por um grupo de jovens apaixonados pelo esporte, inspirados pelo sucesso de outros clubes paulistas. Desde então, o clube se tornou uma força dominante no cenário do futebol brasileiro.

No início de sua trajetória, o São Paulo FC adotou as cores preto, branco e vermelho, mas em 1935, mudou para as cores atuais, preto, branco e vermelho. O emblema do clube também passou por diversas modificações até adotar o formato atual, com a famosa estrela vermelha de cinco pontas.

A década de 1940 foi marcada pela construção do Morumbi, o lendário estádio do São Paulo FC, que se tornou um dos maiores e mais emblemáticos do Brasil. A inauguração do estádio ocorreu em 1960, simbolizando o crescimento e a ambição do clube.

Nos anos 60, o São Paulo FC começou a trilhar seu caminho de sucesso no cenário nacional. Em 1977, o clube conquistou seu primeiro título brasileiro, marcando o início de uma era vitoriosa. Nas décadas seguintes, o São Paulo FC se destacou como um dos times mais dominantes do país, conquistando inúmeros títulos estaduais, nacionais e internacionais.

Um dos momentos mais memoráveis da história do São Paulo FC ocorreu em 1992, quando o clube conquistou sua primeira Copa Libertadores da América. O time, comandado pelo lendário Telê Santana, derrotou o Newell's Old Boys na final em uma emocionante disputa por pênaltis.

No mesmo ano, o São Paulo FC enfrentou o Barcelona na final da Copa Intercontinental e venceu, tornando-se campeão mundial pela primeira vez. Essas conquistas marcaram uma época dourada para o clube, que também venceu a Libertadores novamente em 1993 e 2005.

O São Paulo FC também é conhecido por sua forte base e por revelar talentos que brilharam no futebol brasileiro e internacional, como Raí, Kaká, Cafu, Rogério Ceni, entre outros.

Ao longo de sua história, o São Paulo FC construiu uma rica tradição de vitórias e é considerado um dos clubes mais respeitados do Brasil. Sua paixão, seu espírito competitivo e suas conquistas no futebol fazem dele um dos clubes mais queridos e admirados por torcedores de todo o mundo, deixando uma marca indelével no cenário esportivo brasileiro e mundial.




